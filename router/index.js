import express from 'express';
import json  from 'body-parser';

export const router = express.Router();
import alumnosDb from '../models/alumnos.js';

router.get('/',(req, res)=>{
    res.render('index',{titulo:"Mis Practicas js",nombre:"Mateo Arias Tirado"})
})

router.get('/tabla',(req, res)=>{
//parametros
const params = {
    numero:req.query.numero
}    
res.render('tabla',params)
})

router.post("/tabla", (req, res) => {
  //parametros
  const params = {
    numero: req.body.numero,
  };
  res.render("tabla", params);
})

router.get('/cotizacion', (req,res)=>{
  //Parametros
  const parameto = {
      folio: req.query.folio,
      descripcion:req.query.descripcion,
      valor: req.query.valor,
      pInicial: req.query.pInicial,
      plazos: req.query.plazos
  }
  res.render('cotizacion', parameto);
})

router.post('/cotizacion', (req,res)=>{
  //Parametros
  const parameto = {
      folio: req.body.folio,
      descripcion:req.body.descripcion,
      valor: req.body.valor,
      pInicial: req.body.pInicial,
      plazos: req.body.plazos
  }
  res.render('cotizacion', parameto);
})
  router.post('/tabla',(req,res)=>{

  })
 /*router.get('/alumnos',(req, res)=>{
  //parametros
  const params = {
  }    
  res.render('alumnos',params)
  })
  */

  let params;
  router.post('/alumnos',async(req,res)=>{
      try{
       params={
          matricula:req.body.matricula,
          nombre:req.body.nombre,
          domicilio:req.body.domicilio,
          sexo:req.body.sexo,
          especialidad:req.body.especialidad   
       }
      const res = await alumnosDb.insertar(params);
  
      } catch(error){
          console.error(error)
          res.status(400).send("sucedio un error:" + error);
      }
      res.redirect('/alumnos');
  });
  
  let rows; 
      router.get('/alumnos',async(reg,res)=>{
      rows = await alumnosDb.mostrarTodos();
      res.render('alumnos',{reg:rows});
  
  })
export default {router}